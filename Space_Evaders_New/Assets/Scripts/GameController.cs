﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class GameController : MonoBehaviour
{
	public GameObject hazard;
	public GameObject collectable;
	public GameObject health;
	public GameObject comet;
    public GameObject waveBanner;
    public GameObject waveOne;
    public GameObject showWaveNumber;
    public GameObject largeAsteroid;


	public Vector3 spawnValues;
	public int hazardCount;
	public int collectableCount;
	public int fireCometCount = 0;
    public int largeAsteroidCount = 0;
    public int fireLargeAst = 0;


    public float spawnWait;//Sets the time to spawn collectables & asteroids
	public float startWait;//Short pause when the games starts to give player time to get ready
	public float waveWait;//The amount of time between waves
    public float controlHazardSpawn;

	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;

	private bool gameOver;
	private bool restart;
	public int score;
    public int waveBonusVal = 500;

    public Stopwatch timer;
	public string timeLasted;

    public int mulCount;
    private AddToHS AddToHS;
    private WaveIndicatorCount waveIndicatorCount;
     


    void Start ()
	{
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		score = 0;
		UpdateScore ();
        
        StartCoroutine(NextWave());
        StartCoroutine(FirstWaveDelete());
        StartCoroutine(LowHazardCount());


        InvokeRepeating("SpawnHealth", 10, 10F);
        //InvokeRepeating("SpawnComet", 10, 10F);

        GameObject waveIndicatorCountObject = GameObject.FindWithTag("WaveIndicatorCount");
        if (waveIndicatorCountObject != null)
        {
            waveIndicatorCount = waveIndicatorCountObject.GetComponent<WaveIndicatorCount>();
        }

        /*if (largeAsteroidCount == 1)
        {
            //largeAsteroidCount = 1;
            InvokeRepeating("SpawnLargeAsteroid", 10, 3f);
        }*/

        timer = new Stopwatch();
		timer.Start ();
	}

    //Wave Coroutines --------
    //Wave Coroutines --------
    //Wave Coroutines --------

    IEnumerator FirstWaveDelete ()
    {
        yield return new WaitForSeconds(3.0f);
        Instantiate(waveBanner);
        Instantiate(waveOne);
        yield return new WaitForSeconds(4.0f);
        Destroy(GameObject.Find("Wave Banner(Clone)"));
        Destroy(GameObject.Find("Wave First Count(Clone)"));
    }

    IEnumerator NextWave ()
    {
        yield return new WaitForSeconds(10.0f);
        StartCoroutine(SpawnWaves());
        StartCoroutine(SuspendPlay());
    }

    /*IEnumerator WaveEnd()
    {
        //StopCoroutine(NextWave());
        yield return new WaitForSeconds(20.0f);
        IEnumerator spawnWaves = SpawnWaves();
        StopCoroutine(spawnWaves);
        StartCoroutine(NextWave());
    }*/

    /*IEnumerator WaveIndicator ()
    {
        while (true)
        {
            yield return new WaitForSeconds(43.0f);
            Instantiate(waveBanner);
            StartCoroutine(WaveIndicatorDestroy());
            yield return null;
        }
    }*/

    IEnumerator WaveIndicatorDestroy()
    {
        yield return new WaitForSeconds(4.0f);
        Destroy(GameObject.Find("Wave Banner(Clone)"));
        Destroy(GameObject.Find("Wave Number(Clone)"));
    }

    IEnumerator SuspendPlay()
    {
        while (true)
        {
            yield return new WaitForSeconds(30.0f);
            spawnWait = 0;
            hazardCount = 0;
            yield return new WaitForSeconds(3.0f);
            AddScore(waveBonusVal);
            Instantiate(waveBanner);
            Instantiate(showWaveNumber);
            StartCoroutine(WaveIndicatorDestroy());
            yield return new WaitForSeconds(7.0f);
            spawnWait += controlHazardSpawn;
            hazardCount = 30;
            yield return null;
        }
    }

    IEnumerator LowHazardCount()
    {
        yield return new WaitForSeconds(45.0f);
        controlHazardSpawn = 1.0f;
        yield return new WaitForSeconds(40.0f);
        controlHazardSpawn = 0.3f;
    }

    /*IEnumerator restartPlay()
    {
        //while (true)
        {
            yield return new WaitForSeconds(10.0f);
            spawnWait = 0.3f;
            hazardCount = 30;
            cometCount = 1;
            // score += 100; not working
            waveCount += 1;
            //yield return null;
        }
    }*/
    //----------- Wave Coroutines
    //----------- Wave Coroutines
    //----------- Wave Coroutines

    IEnumerator SpawnWaves ()
	{
        yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < hazardCount; i++, fireCometCount++)
			{
				//var asteroidY = Random.Range (Mathf.RoundToInt(-spawnValues.y), Mathf.RoundToInt(spawnValues.y));
				//var collectY = Random.Range (Mathf.RoundToInt(-spawnValues.y), Mathf.RoundToInt(spawnValues.y));
				var random = Random.Range (0,3) + 1;

                //Debug.Log (Mathf.RoundToInt(-spawnValues.y) + " - " + Mathf.RoundToInt(spawnValues.y));

                //if (asteroidY == collectY) {
                if (random == 1)
                {
                    Vector3 spawnPosition = new Vector3(spawnValues.x, Random.Range(-spawnValues.y, spawnValues.y), spawnValues.z);
                    Quaternion spawnRotation = Quaternion.identity;
                    Instantiate(hazard, spawnPosition, spawnRotation);
                    yield return new WaitForSeconds(spawnWait);
                }
                else if (random == 2)
                {
                    Vector3 spawnPosition = new Vector3(spawnValues.x, Random.Range(-spawnValues.y, spawnValues.y), spawnValues.z);
                    Quaternion spawnRotation = Quaternion.identity;
                    Instantiate(collectable, spawnPosition, spawnRotation);
                    yield return new WaitForSeconds(spawnWait);
                }
                if (fireCometCount == 20)
                {
                    Vector3 spawnPosition = new Vector3(spawnValues.x, Random.Range(-spawnValues.y, spawnValues.y), spawnValues.z);
                    Quaternion spawnRotation = Quaternion.identity;
                    Instantiate(comet, spawnPosition, spawnRotation);
                    fireCometCount = 0;
                    yield return new WaitForSeconds(spawnWait);
                }
            }

                yield return new WaitForSeconds(waveWait);

            if (gameOver)
			{
				restartText.text = "Restart";
				restart = true;
				//break;
			}
        }
    }

//	IEnumerator SpawnCollectables ()
//	{
//		yield return new WaitForSeconds (startWait);
//		while (true)
//		{
//			for (int i = 0; i < collectableCount; i++)
//			{
//				Vector3 spawnPosition = new Vector3 (spawnValues.x, Random.Range (-spawnValues.y, spawnValues.y), spawnValues.z);
//				Quaternion spawnRotation = Quaternion.identity;
//				Instantiate (collectable, spawnPosition, spawnRotation);
//				yield return new WaitForSeconds (spawnWait);
//			}
//			yield return new WaitForSeconds (waveWait);
//
//			if (gameOver)
//			{
//				restartText.text = "Restart";
//				restart = true;
//				break;
//			}
//		}
//	}

//	IEnumerator SpawnComet ()
//	{
//		yield return new WaitForSeconds (startWait);
//		while (true)
//		{
//			for (int i = 0; i < cometCount; i++)
//			{
//				Vector3 spawnPosition = new Vector3 (spawnValues.x, Random.Range (-spawnValues.y, spawnValues.y), spawnValues.z);
//				Quaternion spawnRotation = Quaternion.identity;
//				Instantiate (comet, spawnPosition, spawnRotation);
//				yield return new WaitForSeconds (spawnWait);
//			}
//			yield return new WaitForSeconds (waveWait);
//			
//			if (gameOver)
//			{
//				restartText.text = "Restart";
//				restart = true;
//				break;
//			}
//		}
//	}

	void SpawnHealth ()//This Method works (Y)
	{
		Vector3 spawnPosition = new Vector3 (spawnValues.x, Random.Range (-spawnValues.y, spawnValues.y), spawnValues.z);
		Quaternion spawnRotation = Quaternion.identity;
        Instantiate(health, spawnPosition, spawnRotation);
		if (gameOver) 
		{
			restartText.text = "Restart";
			restart = true;
			
		}
		
	}
	
	public void SpawnLargeAsteroid ()
	{
        
            Vector3 spawnPosition = new Vector3(spawnValues.x, Random.Range(-spawnValues.y, spawnValues.y), spawnValues.z);
            Quaternion spawnRotation = Quaternion.identity;

            fireLargeAst++;
            if ((largeAsteroidCount == 1)&& (fireLargeAst == 3))
            {
                Instantiate(largeAsteroid, spawnPosition, spawnRotation);
                fireLargeAst = 0;
            }

            if (gameOver)
            {
                restartText.text = "Restart";
                restart = true;
            }
 
		
	}


	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
        mulCount ++;

    }

    void UpdateScore ()
	{
		scoreText.text = " " + score;
        PlayerPrefs.SetInt("LatestScore", score);
    }

	public void GameOver ()
	{
		gameOverText.text = "Game Over!";
		gameOver = true;
		CancelInvoke();

		timeLasted = timer.Elapsed.ToString();
		timeLasted = timeLasted.Remove (10, 5);
        PlayerPrefs.SetString("LatestTime", timeLasted);
	}

	public void OnGUI ()
	{
	if (gameOver)
	{
		//if(GUI.Button(new Rect(Screen.width/2.5f,Screen.height/3,Screen.width/5,Screen.height/10), "Restart Game"))
		{
			Application.LoadLevel(5) ;
		}
		
		//if(GUI.Button(new Rect(Screen.width/2.5f,Screen.height/2,Screen.width/5,Screen.height/10), "Main Menu"))
		{
			//Application.LoadLevel(0) ;
		}
	}
	}

}
