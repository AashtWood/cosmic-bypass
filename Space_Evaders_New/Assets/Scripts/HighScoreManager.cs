﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



/// <summary>
/// High score manager.
/// Local highScore manager for LeaderboardLength number of entries
/// 
/// this is a singleton class.  to access these functions, use HighScoreManager._instance object.
/// eg: HighScoreManager._instance.SaveHighScore("meh",1232);
/// No need to attach this to any game object, thought it wouldnot create errors attaching.
/// </summary>

public class HighScoreManager : MonoBehaviour
{
	
	private static HighScoreManager m_instance;
	public static int LeaderboardLength = 5;
	
	public static HighScoreManager _instance {
		get {
			if (m_instance == null) {
				m_instance = new GameObject ("HighScoreManager").AddComponent<HighScoreManager> ();				
			}
			return m_instance;
		}
	}

    public static int LeaderBoardLength { get; internal set; }

    void Awake ()
	{
		if (m_instance == null) {
			m_instance = this;			
		} else if (m_instance != this)		
			Destroy (gameObject);	
		
		DontDestroyOnLoad (gameObject);
	}
	
	//saves the high score..
	public void SaveHighScore (string date, int score, string time)
	{
		List<Scores> HighScores = new List<Scores> ();
		
		int i = 1;
		while (i<=LeaderboardLength && PlayerPrefs.HasKey("HighScore"+i+"score")) {
			Scores temp = new Scores ();
			temp.score = PlayerPrefs.GetInt ("HighScore" + i + "score");
			temp.date = PlayerPrefs.GetString ("HighScore" + i + "date");
			temp.time = PlayerPrefs.GetString ("HighScore" + i + "time");
			HighScores.Add (temp);
			i++;
		}
		if (HighScores.Count == 0) { 			
			Scores _temp = new Scores ();
			_temp.date = date;
			_temp.score = score;
			_temp.time = time;
			HighScores.Add (_temp);
		} else {
			//Organizing the high score list with the highest at the top.
			for (i=1; i<=HighScores.Count && i<=LeaderboardLength; i++) {
				if (score > HighScores [i - 1].score) {
					Scores _temp = new Scores ();
					_temp.date = date;
					_temp.score = score;
					_temp.time = time;
					HighScores.Insert (i - 1, _temp);
					break;
				}			
				if (i == HighScores.Count && i < LeaderboardLength) {
					Scores _temp = new Scores ();
					_temp.date = date;
					_temp.score = score;
					HighScores.Add (_temp);
					break;
				}
			}
		}
		
		i = 1;
		while (i<=LeaderboardLength && i<=HighScores.Count) {
			PlayerPrefs.SetString ("HighScore" + i + "date", HighScores [i - 1].date);
			PlayerPrefs.SetInt ("HighScore" + i + "score", HighScores [i - 1].score);
			PlayerPrefs.SetString ("HighScore" + i + "time", HighScores [i - 1].time);
			i++;
		}
	}

    //Gives you a list of all available highscores.
    public List<Scores>  GetHighScore ()
	{
		List<Scores> HighScores = new List<Scores> ();
		
		int i = 1;
		while (i<=LeaderboardLength && PlayerPrefs.HasKey("HighScore"+i+"score")) {
			Scores temp = new Scores ();
			temp.score = PlayerPrefs.GetInt ("HighScore" + i + "score");
			temp.date = PlayerPrefs.GetString ("HighScore" + i + "date");
			temp.time = PlayerPrefs.GetString ("HighScore" + i + "time");
			HighScores.Add (temp);
			i++;
		}
		return HighScores;
	}

    //clears all the high scores already saved. (useful for testing mostly!!)
    public void ClearLeaderBoard ()
	{
		List<Scores> HighScores = GetHighScore();
		
		for(int i=1;i<=HighScores.Count;i++)
		{
			PlayerPrefs.DeleteKey("HighScore" + i + "date");
			PlayerPrefs.DeleteKey("HighScore" + i + "score");
			PlayerPrefs.DeleteKey("HighScore" + i + "time");

		}
	}
	
	//I dont think we need this anymore after unity 3.5
	void OnApplicationQuit()
	{
		PlayerPrefs.Save();
	}
}

public class Scores
{
	public int score;
	public string date;
	public string time;

	
}