﻿using UnityEngine;
using System.Collections;

public class WaveIndicatorCount : MonoBehaviour {

    private GameController gameController;
    public GUIText waveNumber;
    public int WaveCount = 0;
    public float nextActionTime = 0.0f;
    public float period = 30.0f;


    void Start()
    {
        InvokeRepeating("WaveCounter", 10f, 1);

        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }

        
    }

    void WaveCounter ()
    {
	    if (Time.timeSinceLevelLoad > nextActionTime)
        {
            nextActionTime += period + 10;
            WaveCount++;
            waveNumber.text = "" + WaveCount;
        }

        if (WaveCount == 5)
        {
            StartCoroutine(LargeAsteroidWave());
        }
    }

    IEnumerator LargeAsteroidWave()
    {
        yield return new WaitForSeconds(8f);
        gameController.spawnWait = 0.0f;
        gameController.hazardCount = 0;
        gameController.largeAsteroidCount = 1;
        gameController.SpawnLargeAsteroid();

        if (WaveCount == 6)
        {
            gameController.largeAsteroidCount = 0;
            gameController.fireLargeAst = 0;
        }
    }
}



//Wave 1 = 10s
//Wave 1 E = 40s
//Wave 2 B = 50s
//Wave 2 E = 80s
//Wave 3 B = 90s
//Wave 3 E = 120s
//Wave 4 B = 130s
//Wave 4 E = 160s

//SU = 45s
//SU = 85s
//SU = 125s
//SU = 165s