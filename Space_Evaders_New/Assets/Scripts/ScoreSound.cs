﻿using UnityEngine;
using System.Collections;

public class ScoreSound : MonoBehaviour {

	private AudioSource audioSource;
	private AudioSource audioSource1;
	private AudioSource audioSource2;
	private AudioSource audioSource3;
	public AudioClip Scored1;
	public AudioClip Scored2;
	public AudioClip Health;
	public AudioClip Asteroid;

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "collectable")
		{
			audioSource = GetComponent<AudioSource>();
			audioSource.clip = Scored1;
			audioSource.Play();

			audioSource1 = GetComponent<AudioSource>();
			audioSource1.clip = Scored2;
			audioSource1.Play();
		}

		if (other.tag == "Health")
		{
			audioSource2 = GetComponent<AudioSource>();
			audioSource2.clip = Health;
			audioSource2.Play();
		}

		if (other.tag == "Hazard")
		{
			audioSource3 = GetComponent<AudioSource>();
			audioSource3.clip = Asteroid;
			audioSource3.Play();
		}
	}
}
