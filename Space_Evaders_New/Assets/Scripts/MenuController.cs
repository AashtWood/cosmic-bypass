﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuController : MonoBehaviour {

	public GUISkin guiSkin;
	public Font yourFont;
    public int guiDepth;
    private static int lengthOfLeaderboard;
	GUIStyle customButtonStyle;


	string date= "";
	string score="";
	string time="";
	List<Scores> highscore;
	
	// Use this for initialization
	void Start () {
		//EventManager._instance._buttonClick += ButtonClicked;
		
		highscore = new List<Scores>();
        lengthOfLeaderboard = HighScoreManager.LeaderBoardLength;

    }
	
	
	void ButtonClicked(GameObject _obj)
	{
		print("Clicked button:"+_obj.name);
	}
	
	// Update is called once per frame
	void Update ()
    {

	}
	
	void OnGUI()
	{
		GUI.skin = guiSkin;
		if (customButtonStyle == null) {
			customButtonStyle = new GUIStyle(GUI.skin.button);
			customButtonStyle.font = yourFont;
            customButtonStyle.alignment = TextAnchor.UpperLeft;
        }
		/*GUILayout.BeginHorizontal();
		GUILayout.Label("Date :");
		date =  GUILayout.TextField(date);
		GUILayout.EndHorizontal();
		
		GUILayout.BeginHorizontal();
		GUILayout.Label("Score :");
		score =  GUILayout.TextField(score);
		GUILayout.EndHorizontal();

		
		if(GUILayout.Button("Add Score"))
		{
			HighScoreManager._instance.SaveHighScore(date,System.Int32.Parse(score));
			highscore = HighScoreManager._instance.GetHighScore();    
		}
		
		if(GUILayout.Button("Get LeaderBoard"))
		{
			highscore = HighScoreManager._instance.GetHighScore();            
		}
		*/
		if(GUILayout.Button("Clear Leaderboard"))
		{
			HighScoreManager._instance.ClearLeaderBoard();            
		}
		//if(GUI.Button(new Rect(Screen.width/2.5f,Screen.height/2,Screen.width/5,Screen.height/10), "Back"))
		{
			//Application.LoadLevel(0) ;
		}
		
		GUILayout.Space(60);
		highscore = HighScoreManager._instance.GetHighScore();            

		GUILayout.BeginHorizontal();
		/*GUILayout.Label("Date",GUILayout.Width(Screen.width/3));
		GUILayout.Label("Scores",GUILayout.Width(Screen.width/3));
		GUILayout.Label("Time",GUILayout.Width(Screen.width/3));*/



		GUILayout.EndHorizontal();
		
		GUILayout.Space(110);

        foreach (Scores _score in highscore)
        {

            GUILayout.BeginHorizontal();
            GUILayout.Label("                                               " + _score.score, customButtonStyle, GUILayout.Width(Screen.width / 2f));
            GUILayout.Label("            " + _score.time, customButtonStyle, GUILayout.Width(Screen.width / 2f));
            //GUILayout.Label("" + _score.score, customButtonStyle, GUILayout.Width(Screen.width / 3)); //GUILayout.Height(Screen.height/1.5f -370));
            //GUILayout.Label("" + _score.time, customButtonStyle, GUILayout.Width(Screen.width / 1.5f));// GUILayout.Height(Screen.height / 4f));
            GUILayout.EndHorizontal();
			
		

        }

    }
}