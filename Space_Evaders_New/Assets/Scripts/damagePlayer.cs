﻿using UnityEngine;
using System.Collections;

public class damagePlayer : MonoBehaviour {

	public ParticleSystem collisionEffect;
	public ParticleSystem smoke;
	public ParticleSystem smoke1;
	public ParticleSystem fire;
	public ParticleSystem fire1;
	public GameObject fullHp;
	public GameObject barFull;
	public GameObject HalfHp;
	public GameObject barHalf;
	public int playerHealth = 100;
	public int scoreValue;
	int damage = 50;
	string date1 = System.DateTime.Now.ToString();

	private GameController gameController;


	void Start()
	{
		print (playerHealth);

		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent <GameController>();
		}
		if (gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}


		if (playerHealth == 100)
		{

			{
				GameObject go = Instantiate(fullHp);
			}
			{
				GameObject go = Instantiate(barFull);
			}
		}

	}

	void OnTriggerEnter(Collider other) 
	{
		if (other.tag == "Hazard") 
		{
			playerHealth -=damage;
			print(playerHealth);
				{
					GameObject go = Instantiate(HalfHp);
				}
				
				{
					GameObject go = Instantiate(barHalf);
				}
			Destroy(GameObject.Find("HUD100HP(Clone)"));
			Destroy(GameObject.Find("HUDHPBarFull(Clone)"));
		}

		if (other.tag == "Asteroid")
		{
			Destroy(other.gameObject);
			Destroy(gameObject);
			gameController.GameOver();
			HighScoreManager._instance.SaveHighScore(date1,gameController.score, gameController.timeLasted);
		}

		if (other.tag == "Health") 
		{
			if(playerHealth==50)
			{
			playerHealth +=damage;
			print (playerHealth);
					{
						GameObject go = Instantiate(fullHp);
					}
					{
						GameObject go = Instantiate(barFull);
					}
			Destroy(GameObject.Find("HUD50HP(Clone)"));
			Destroy(GameObject.Find("HUDHPBarHalf(Clone)"));
			}
		}

		if (playerHealth == 0)
		{
			Destroy(other.gameObject);
			Destroy(gameObject);
			gameController.GameOver();
			HighScoreManager._instance.SaveHighScore(date1,gameController.score, gameController.timeLasted);

		}

		if (playerHealth == 100)
		{
			collisionEffect.Stop();
			smoke.Stop();
			smoke1.Stop();
			fire.Stop();
			fire1.Stop();
		}

		if (playerHealth == 50)
		{
			collisionEffect.Play();
			smoke.Play();
			smoke1.Play();
			fire.Play();
			fire1.Play();
		}
	}
}
