﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour
{
    public float speed;

    public float nextActionTime = 5f;
    public float period = 40f;

    void Update()
    {
        if (Time.timeSinceLevelLoad > nextActionTime)
        {
            if (speed != -30)
            {
                nextActionTime += period;
                speed = speed - 1.5f;
                GetComponent<Rigidbody>().velocity = transform.right * speed;
            }
        }
    }
}
