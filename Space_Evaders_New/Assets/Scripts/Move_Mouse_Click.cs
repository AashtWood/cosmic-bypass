﻿using UnityEngine;
using System.Collections;

public class Move_Mouse_Click : MonoBehaviour {

	private Vector3 screenPoint;
	private Vector3 offset;
	
	/***Slider***/
	void OnMouseDown(){

		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(2, Input.mousePosition.y));

	}

	void OnMouseDrag(){

		Vector3 cursorPoint = new Vector3(2, Input.mousePosition.y);
		Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
		transform.position = cursorPosition;

		/***Lock down slider***/
		Vector3 pos = transform.position;
		pos.y = Mathf.Clamp(pos.y , -4, 4);
		transform.position = pos;

	}


}
