﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour 

{
	public GUISkin guiSkin;
	public GUISkin guiSkin1;
	public GUISkin guiSkin2;
	public GUISkin guiSkin3;
	public Font yourFont;
    public AudioClip menuButtonSound;
    private AudioSource audioSource;
    GUIStyle customButtonStyle;
	GUIStyle customButtonStyle1;
	GUIStyle customButtonStyle2;
	GUIStyle customButtonStyle3;
    static bool AudioBegin = false;

    void Update()
    {
        if (!AudioBegin)
        {
            if (PlayerPrefs.GetFloat("mutestate") == 1)
            {
                AudioListener.volume = 1.0f;
                AudioBegin = true;
            }
            else if (PlayerPrefs.GetFloat("mutestate") == 0)
            {
                AudioListener.volume = 0.0f;
                AudioBegin = false;
            }


        }

    }

    IEnumerator HighScoreScreen()
    {
        yield return new WaitForSeconds(0.15f);
        Application.LoadLevel(6);
    }

    IEnumerator OptionsScreen()
    {
        yield return new WaitForSeconds(0.15f);
        Application.LoadLevel(3);
    }

    void OnGUI() {

		GUI.skin = guiSkin;
		if (customButtonStyle == null) {
			customButtonStyle = new GUIStyle(GUI.skin.button);
			customButtonStyle.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2)-65,Screen.height/3,130,50), "PLAY", customButtonStyle))
		{
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = menuButtonSound;
            audioSource.Play();
            Application.LoadLevel(1) ;
		}

		GUI.skin = guiSkin1;
		if (customButtonStyle1 == null) {
			customButtonStyle1 = new GUIStyle(GUI.skin.button);
			customButtonStyle1.font = yourFont;
		}

        if (GUI.Button(new Rect((Screen.width / 2) - 125, Screen.height / 2.25f, 250, 50), "HIGH SCORES", customButtonStyle1))
        {
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = menuButtonSound;
            audioSource.Play();
            StartCoroutine(HighScoreScreen());
        }
        
		GUI.skin = guiSkin2;
		if (customButtonStyle2 == null) {
			customButtonStyle2 = new GUIStyle(GUI.skin.button);
			customButtonStyle2.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2)-90,Screen.height/1.8f,180,50), "OPTIONS", customButtonStyle2))
		{
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = menuButtonSound;
            audioSource.Play();
            StartCoroutine(OptionsScreen());
		}

		GUI.skin = guiSkin3;
		if (customButtonStyle3 == null) {
			customButtonStyle3 = new GUIStyle(GUI.skin.button);
			customButtonStyle3.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2)-60,Screen.height/1.5f,120,50), "EXIT", customButtonStyle3))
		{
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = menuButtonSound;
            audioSource.Play();
            Application.Quit() ;
		}

	}
}
