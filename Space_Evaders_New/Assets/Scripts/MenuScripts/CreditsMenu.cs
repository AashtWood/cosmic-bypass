﻿using UnityEngine;
using System.Collections;

public class CreditsMenu : MonoBehaviour 

{
	public GUISkin guiSkin;
	public GUISkin guiSkin1;
	public Font yourFont;
    public AudioClip menuButtonSound;
    private AudioSource audioSource;
    GUIStyle customButtonStyle;
	GUIStyle customButtonStyle1;

    IEnumerator BackScreen()
    {
        yield return new WaitForSeconds(0.15f);
        Application.LoadLevel(3);
    }

    void OnGUI() {
		GUI.skin = guiSkin;
		if (customButtonStyle == null) {
			customButtonStyle = new GUIStyle(GUI.skin.button);
			customButtonStyle.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2)-125,Screen.height/3,250,50), "JOSH WOOD", customButtonStyle))
		{
			//Application.LoadLevel(1) ;
		}

		//if(GUI.Button(new Rect(Screen.width/2.5f,Screen.height/2.25f,Screen.width/5,Screen.height/10), "HINTS", customButtonStyle))
		{
			//Application.LoadLevel(2) ;
		}

		if(GUI.Button(new Rect((Screen.width/2)-135,Screen.height/2,270,50), "OLIVER AASHT", customButtonStyle))
		{
			//Application.LoadLevel(3) ;
		}

		GUI.skin = guiSkin1;
		if (customButtonStyle1 == null) {
			customButtonStyle1 = new GUIStyle(GUI.skin.button);
			customButtonStyle1.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2)-65,Screen.height/1.25f,130,50), "BACK", customButtonStyle1))
		{
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = menuButtonSound;
            audioSource.Play();
            StartCoroutine(BackScreen());
        }
	}

}
