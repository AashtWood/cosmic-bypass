﻿using UnityEngine;
using System.Collections;

public class OptionsMenu : MonoBehaviour 

{
	public GUISkin guiSkin;
	public GUISkin guiSkin1;
	public GUISkin guiSkin2;
	public GUISkin guiSkin3;
	public GUISkin guiSkin4;
	public Font yourFont;
    public AudioClip menuButtonSound;
    private AudioSource audioSource;
    GUIStyle customButtonStyle;
	GUIStyle customButtonStyle1;
	GUIStyle customButtonStyle2;
	GUIStyle customButtonStyle3;
	GUIStyle customButtonStyle4;
    public bool toggleImg = false;
	public bool toggleImg2 = false;
    public float mute;

  //  public int isSoundOn;

    void Start()
    {
        mute = PlayerPrefs.GetFloat("mutestate");
        if (mute == 0.0f)
        {
            toggleImg = true;
        }
        else
        {
            toggleImg = false;
        }
    }

    IEnumerator CreditsScreen()
    {
        yield return new WaitForSeconds(0.15f);
        Application.LoadLevel(4);
    }

    IEnumerator BackScreen()
    {
        yield return new WaitForSeconds(0.15f);
        Application.LoadLevel(0);
    }

    void OnGUI() {

        /*if (gameObject.GetComponent <OptionsMenu> ().toggleImg)
        {
            Debug.Log("It's true!");
        }
        if (!gameObject.GetComponent <OptionsMenu>().toggleImg)
        {
            Debug.Log("It's false!");
        }*/
		
        GUI.skin = guiSkin;
		if (customButtonStyle == null) {
			customButtonStyle = new GUIStyle(GUI.skin.button);
			customButtonStyle.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2)-75,Screen.height/3,150,50), "SOUND", customButtonStyle))
		{
			//Application.LoadLevel(1) ;
		}

		if(GUI.Button(new Rect((Screen.width/2)-75,Screen.height/2,150,50), "HINTS", customButtonStyle))
		{
			//Application.LoadLevel(2) ;
		}

		GUI.skin = guiSkin1;
		if (customButtonStyle1 == null) {
			customButtonStyle1 = new GUIStyle(GUI.skin.button);
			customButtonStyle1.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2)-90,Screen.height/1.5f,180,50), "CREDITS", customButtonStyle1))
		{
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = menuButtonSound;
            audioSource.Play();
            StartCoroutine(CreditsScreen());
        }

		GUI.skin = guiSkin2;
		if (customButtonStyle2 == null) {
			customButtonStyle2 = new GUIStyle(GUI.skin.button);
			customButtonStyle2.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2)-65,Screen.height/1.25f,130,50), "BACK", customButtonStyle2))
		{
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = menuButtonSound;
            audioSource.Play();
            StartCoroutine(BackScreen());
        }

		GUI.skin = guiSkin3;
		if (customButtonStyle3 == null) {
			customButtonStyle3 = new GUIStyle(GUI.skin.toggle);
			customButtonStyle3.font = yourFont;
		}

        toggleImg = GUI.Toggle(new Rect((Screen.width / 2.6f) - 30, Screen.height / 3, 60, 50), toggleImg, "", customButtonStyle3);
        {
            if (toggleImg == false)
            {               
                AudioListener.volume = 1.0f;
                PlayerPrefs.SetFloat("mutestate", 1.0f);
               // isSoundOn = 1;
            }
            if (toggleImg == true)
            {               
                AudioListener.volume = 0.0f;
                PlayerPrefs.SetFloat("mutestate", 0.0f);
                //isSoundOn = 0;
            }
        }

        GUI.skin = guiSkin4;
		if (customButtonStyle4 == null) {
			customButtonStyle4 = new GUIStyle(GUI.skin.toggle);
			customButtonStyle4.font = yourFont;
		}

		if (toggleImg2 = GUI.Toggle(new Rect((Screen.width/2.6f)-30,Screen.height/2,60,50), toggleImg2, "", customButtonStyle4))
			//(Screen.width/2.75f,Screen.height/2.95f,Screen.width/14,Screen.height/12)
		{
			
		}
	}

}
