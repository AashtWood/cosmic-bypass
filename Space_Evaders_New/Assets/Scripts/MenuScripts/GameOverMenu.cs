﻿using UnityEngine;
using System.Collections;

public class GameOverMenu : MonoBehaviour 

{
	public GUISkin guiSkin;
	public GUISkin guiSkin1;
	public GUISkin guiSkin2;
	public Font yourFont;
    public AudioClip menuButtonSound;
    private AudioSource audioSource;
    GUIStyle customButtonStyle;
	GUIStyle customButtonStyle1;
	GUIStyle customButtonStyle2;

    IEnumerator RestartScreen()
    {
        yield return new WaitForSeconds(0.15f);
        Application.LoadLevel(1);
    }

    IEnumerator MainMenuScreen()
    {
        yield return new WaitForSeconds(0.15f);
        Application.LoadLevel(0);
    }


    void OnGUI() {
		GUI.skin = guiSkin;
		if (customButtonStyle == null) {
			customButtonStyle = new GUIStyle(GUI.skin.button);
			customButtonStyle.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2.75f)-75,Screen.height/3.25f,150,50), "SCORE", customButtonStyle))
		{
			//Application.LoadLevel(1) ;
		}

		if(GUI.Button(new Rect((Screen.width/1.55f)-75,Screen.height/3.25f,150,50), "TIME", customButtonStyle))
		{
			//Application.LoadLevel(2) ;
		}

		GUI.skin = guiSkin1;
		if (customButtonStyle1 == null) {
			customButtonStyle1 = new GUIStyle(GUI.skin.button);
			customButtonStyle1.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2)-95,Screen.height/1.75f,190,50), "RESTART", customButtonStyle1))
		{
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = menuButtonSound;
            audioSource.Play();
            StartCoroutine(RestartScreen());
        }

		GUI.skin = guiSkin2;
		if (customButtonStyle2 == null) {
			customButtonStyle2 = new GUIStyle(GUI.skin.button);
			customButtonStyle2.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2)-115,Screen.height/1.25f,230,50), "MAIN MENU", customButtonStyle2))
		{
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = menuButtonSound;
            audioSource.Play();
            StartCoroutine(MainMenuScreen());
        }
	}

}
