﻿using UnityEngine;
using System.Collections;

public class HighScoresMenu : MonoBehaviour 

{
	public GUISkin guiSkin;
	public GUISkin guiSkin1;
	public Font yourFont;
    public AudioClip menuButtonSound;
    private AudioSource audioSource;
    GUIStyle customButtonStyle;
	GUIStyle customButtonStyle1;

    IEnumerator BackScreen()
    {
        yield return new WaitForSeconds(0.15f);
        Application.LoadLevel(0);
    }

    void OnGUI() {
		GUI.skin = guiSkin;
		if (customButtonStyle == null) {
			customButtonStyle = new GUIStyle(GUI.skin.button);
			customButtonStyle.font = yourFont;
		}
		
		//if(GUI.Button(new Rect(Screen.width/2.5f,Screen.height/3.8f,Screen.width/5,Screen.height/10), "SCORE", customButtonStyle))
		{
			//Application.LoadLevel(1) ;
		}

		if(GUI.Button(new Rect((Screen.width/1.6f)-75,Screen.height/4,150,50), "TIME", customButtonStyle))
		{
			//Application.LoadLevel(2) ;
		}

		if(GUI.Button(new Rect((Screen.width/2.6f)-75,Screen.height/4,150,50), "SCORE", customButtonStyle));
		{
			//Application.LoadLevel(1) ;
		}

		GUI.skin = guiSkin1;
		if (customButtonStyle1 == null) {
			customButtonStyle1 = new GUIStyle(GUI.skin.button);
			customButtonStyle1.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2)-65,Screen.height/1.15f,130,50), "BACK", customButtonStyle1))
		{
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = menuButtonSound;
            audioSource.Play();
            StartCoroutine(BackScreen());
        }
	}

}
