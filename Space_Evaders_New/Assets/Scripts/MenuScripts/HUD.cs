﻿using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour 

{
	public GUISkin guiSkin;
	public Font yourFont;
	GUIStyle customButtonStyle;
	void OnGUI() {
		GUI.skin = guiSkin;
		if (customButtonStyle == null) {
			customButtonStyle = new GUIStyle(GUI.skin.button);
			customButtonStyle.font = yourFont;
		}

		if(GUI.Button(new Rect((Screen.width/2)-75,Screen.height/3,150,50), "SCORE", customButtonStyle))
		{
			//Application.LoadLevel(1) ;
		}

		if(GUI.Button(new Rect(Screen.width/2.05f,Screen.height/100f,Screen.width/5,Screen.height/10), "TIME", customButtonStyle))
		{
			//Application.LoadLevel(2) ;
		}

		//if(GUI.Button(new Rect(Screen.width/2.5f,Screen.height/1.8f,Screen.width/5,Screen.height/10), "RESTART", customButtonStyle))
		{
			//Application.LoadLevel(1) ;
		}

		//if(GUI.Button(new Rect(Screen.width/2.5f,Screen.height/1.5f,Screen.width/5,Screen.height/10), "MAIN MENU", customButtonStyle))
		{
			//Application.LoadLevel(0) ;
		}
	}

}
