﻿using UnityEngine;
using System.Collections;

public class menuMusic : MonoBehaviour
{

    static bool AudioBegin = false;

    void Update()
    {

        if (Application.loadedLevelName == "First_Scene")
        {
            Destroy(this.gameObject);

        }

        if (!AudioBegin)
        {
            if (PlayerPrefs.GetFloat("mutestate") == 1)
            {
                AudioListener.volume = 1.0f;
                DontDestroyOnLoad(gameObject);
                AudioBegin = true;
            }
            else if (PlayerPrefs.GetFloat("mutestate") == 0)
            {
                AudioListener.volume = 0.0f;
                DontDestroyOnLoad(gameObject);
                AudioBegin = false;
            }


        }

    }
}
