﻿using UnityEngine;
using System.Collections;

public class CameraCheckRes : MonoBehaviour {

	// Use this for initialization
	void Start () {

		Camera cam = Camera.main;
		float height = 2f * cam.orthographicSize;
		float width = height * cam.aspect;
	}
}
