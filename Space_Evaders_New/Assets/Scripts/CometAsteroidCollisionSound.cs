﻿using UnityEngine;
using System.Collections;

public class CometAsteroidCollisionSound : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip Asteroid;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hazard")
        {
            audioSource = GetComponent<AudioSource>();
            audioSource.clip = Asteroid;
            audioSource.Play();
        }
    }
}
