﻿using UnityEngine;
using System.Collections;

public class AddToHS : MonoBehaviour
{
	public GameObject explosion;
    public GameObject multiX2;
    public GameObject multiX4;
    public GameObject multiX8;
    public GameObject multiX16;
    public GameObject multiX32;
    public int scoreValue = 10;
	private GameController gameController;
    private bool multiX2On;
    //private AudioSource audioSource;
    //public AudioClip Scored;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }


    void OnTriggerEnter(Collider other)
	{

		if (other.tag == "Boundary")
		{
			return;
		}
		else if(other.tag == "Player")
		{
            
            
            if (gameController.mulCount >= 10)
            {
                scoreValue = 10;
                scoreValue = scoreValue * 2;

                if (gameController.mulCount == 10)
                {
                    Instantiate(multiX2);
                }
              
            }
            

            if (gameController.mulCount >= 20)
            {
                scoreValue = 10;
                scoreValue = scoreValue * 4;

                if (gameController.mulCount == 20)
                {
                    Destroy(GameObject.Find("MultiplierX2(Clone)"));
                    Instantiate(multiX4);
                }

            }
            if (gameController.mulCount >= 30)
            {
                scoreValue = 10;
                scoreValue = scoreValue * 8;

                if (gameController.mulCount == 30)
                {
                    Destroy(GameObject.Find("MultiplierX4(Clone)"));
                    Instantiate(multiX8);
                }

            }
            if (gameController.mulCount >= 40)
            {
                scoreValue = 10;
                scoreValue = scoreValue * 16;

                if (gameController.mulCount == 40)
                {
                    Destroy(GameObject.Find("MultiplierX8(Clone)"));
                    Instantiate(multiX16);
                }

            }
            if (gameController.mulCount >= 50)
            {
                scoreValue = 10;
                scoreValue = scoreValue * 32;

                if (gameController.mulCount == 50)
                {
                    Destroy(GameObject.Find("MultiplierX16(Clone)"));
                    Instantiate(multiX32);
                }

            }
            //audioSource = GetComponent<AudioSource>();
            //audioSource.clip = Scored;
            //audioSource.Play();
            Instantiate(explosion, transform.position, transform.rotation);
			gameController.AddScore (scoreValue);
			Destroy(gameObject);
		}
		else if(other.tag == "Asteroid")
		{
			return;
		}
	}
}