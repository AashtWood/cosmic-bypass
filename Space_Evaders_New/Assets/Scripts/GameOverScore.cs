﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class GameOverScore : MonoBehaviour
{
    public GUIText latestScoreText;
    public GUIText latestTimeText;
    public int latestScore;
    public string latestTime;

    void Start()
    {
        latestScore = PlayerPrefs.GetInt("LatestScore");
        latestTime = PlayerPrefs.GetString("LatestTime");
        UpdateScore();
    }

    void UpdateScore()
    {
        latestScoreText.text = " " + latestScore;
        latestTimeText.text = " " + latestTime;
    }








}


