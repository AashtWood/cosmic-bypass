﻿using UnityEngine;
using System.Collections;

public class Scroll : MonoBehaviour {

	public float speed = 0.5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 offset = new Vector3 (Time.time * speed, 0);


		GetComponent<Renderer>().material.mainTextureOffset = offset;
	
	
	}

}
