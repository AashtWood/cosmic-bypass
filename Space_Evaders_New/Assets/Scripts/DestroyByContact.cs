﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour
{
	//public GameObject explosion;
	public GameObject playerExplosion;
	public int scoreValue;
	private GameController gameController;


    //	string date1 = System.DateTime.Now.ToString();

    /*public Transform asteroidPrefeb;
	public Transform cometPrefeb;
	public Transform collectablePrefeb;*/




    void Start ()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null)
		{
			gameController = gameControllerObject.GetComponent <GameController>();
		}
		if (gameController == null)
		{
			Debug.Log ("Cannot find 'GameController' script");
		}
	}
	
	void OnTriggerEnter(Collider other) 
	{
		//Transform asteroid = Instantiate(asteroidPrefeb) as Transform;
		//Transform comet = Instantiate(cometPrefeb) as Transform;

		//Physics.IgnoreCollision (other.GetComponent<Collider>(), comet.GetComponent<Collider>());
//		Physics.IgnoreCollision (asteroid.GetComponent<Collider>(), collectable.GetComponent<Collider>());


		if (other.tag == "Boundary")
		{
			return;
		}
		if (other.tag == "Asteroid")
		{
            //other.GetComponent<Rigidbody>().velocity = new Vector3(-15,0,0);
            //Instantiate(explosion, other.transform.position, other.transform.rotation);
            Vector3 vSpawnPos = transform.position;
            vSpawnPos.x -= 1;
            Instantiate(playerExplosion, vSpawnPos, Quaternion.Euler(0, 0, -90));
            Destroy(gameObject);
		}
		if (other.tag == "collectable")
		{

		}
		else if(other.tag == "Player")
		{
			print (other.transform.position);
			Vector3 vSpawnPos = other.transform.position;
			vSpawnPos.x += 1;
			vSpawnPos.z -= 1;
			Instantiate(playerExplosion, vSpawnPos, Quaternion.Euler(0, 0, 90));
            gameController.mulCount = 0;
            Destroy(GameObject.Find("MultiplierX2(Clone)"));
            Destroy(GameObject.Find("MultiplierX4(Clone)"));
            Destroy(GameObject.Find("MultiplierX8(Clone)"));
            Destroy(GameObject.Find("MultiplierX16(Clone)"));
            Destroy(GameObject.Find("MultiplierX32(Clone)"));
            //HighScoreManager._instance.SaveHighScore(date1,gameController.score);
            //Destroy(other.gameObject);
            Destroy(gameObject);
			//gameController.GameOver();
		}
		//else if(other.tag == "collectable")
		//{
			
			//Transform collectable = Instantiate(collectablePrefeb) as Transform;


			//return;

		//}
//		gameController.AddScore (scoreValue);


	}
}